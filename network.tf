resource "aws_security_group" "app-nodes" {
  name        = "app-nodes"
  description = "Allow public subnet to access app"
  vpc_id     = var.vpc_id
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.ssh_cidrs
        description = "ssh from a single ip address"
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/8"]
        description = "open ports from everywhere"
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/8"]
        description = "open ports from everywhere"
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        description = "open ports to anywhere"
    }
  tags = {
    Name = "app"
  }
}

resource "aws_security_group" "app-lb" {
  name        = "app-lb"
  description = "Allow public subnet to access app"
  vpc_id     = var.vpc_id
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.ssh_cidrs
        description = "ssh from a single ip address"
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "open ports from everywhere"
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "open ports from everywhere"
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        description = "open ports to anywhere"
    }
  tags = {
    Name = "app"
  }
}