# Log into AWS
provider "aws" {
  #version = "~> 2.50.0"
  #region = var.region
  #access_key = var.AWS_ACCESS_KEY
  #secret_key = var.AWS_SECRET_KEY
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "random_id" "deployment_tag" {
  byte_length = 4
}

resource "aws_key_pair" "generated_key" {
  key_name   = "${random_id.deployment_tag.hex}-key"
  public_key = tls_private_key.ssh.public_key_openssh
}

resource "aws_instance" "app-servers" {
  for_each = toset(var.private_subnets)  #toset(var.private_subnets)
  ami           = var.aws_ami
  instance_type = var.ec2_size
  #count= var.app_count
  subnet_id = each.key
  security_groups = [aws_security_group.app-nodes.id]
  key_name = aws_key_pair.generated_key.key_name
  user_data_base64 = data.template_cloudinit_config.app.rendered
  tags = {
    Name  = "${var.application_name}-${each.key}"
  }
}

resource "aws_lb" "app" {
  name               = var.application_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.app-lb.id]
  subnets            = var.public_subnets

  enable_deletion_protection = false

  tags = {
    Environment = "prod"
  }
}

resource "aws_lb_target_group" "app" {
  name        = "app-80"
  target_type = "instance"
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  port        = 80
}

resource "aws_lb_target_group_attachment" "app" {
  for_each = aws_instance.app-servers
   
    target_group_arn = aws_lb_target_group.app.arn
    target_id        = aws_instance.app-servers[each.key].id
    port             = 80 #443
}

resource "aws_lb_listener" "app" {
  load_balancer_arn = aws_lb.app.arn #var.alb_arn
  port              = var.alb_port
  protocol          = "HTTP"
#  protocol          = "HTTPS"
#  ssl_policy        = var.ssl_policy
#  certificate_arn   = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app.arn
  }
}

data "template_cloudinit_config" "app" {
  gzip         = true
  base64_encode = true
  part {
    filename     = "app.sh" # This could be an install script, config script, etc
    content_type = "text/x-shellscript"
    content = templatefile("${var.app_tpl_filepath_filename}",
      {
        name                      = "name" #placeholder
        config-dir                = "config-dir" #placeholder
        nexus_username            = var.NEXUS_USERNAME,
        nexus_password            = var.NEXUS_PASSWORD,
        app_download_url          = var.app_download_url,
        app_version               = var.app_version,
        http_proxy                = var.http_proxy,
        no_proxy                  = var.no_proxy,
      }
    )
  }
}