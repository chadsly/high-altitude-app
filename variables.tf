variable "region"{
  default = ""
}

variable "AWS_ACCESS_KEY"{
  default = ""
}

variable "AWS_SECRET_KEY"{
  default = ""
}

variable "bastion_host"{
  default = ""
}

variable "aws_ami"{
  default = ""
}

variable "ami_name"{
  default = ""
}

variable "ec2_size"{
  default = ""
}

variable "lb_size"{
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "private_subnets" {
  default = ""
}

variable "public_subnets" {
  default = ""
}

variable "app_count" {
  default= 3
}

variable "key_name" {
  default = ""
}

variable "ssh_cidrs" {
}

variable "alb_arn" {
  description = "ARN for the ALB"
  default = ""
}

variable "alb_port" {
  description = "port to use on the ALB"
  default = ""
}

variable "ssl_policy" {
  default = ""
}

variable "certificate_arn" {
  default = ""
}

variable "application_name" {
  default = ""
}

variable "NEXUS_USERNAME" { default = "" }

variable "NEXUS_PASSWORD" { default = "" }

variable "app_download_url" { default = "" }
variable "app_version" { default = "" }
variable "app_tpl_filepath_filename"  { default = "" }

variable "http_proxy" { default = "" }
variable "no_proxy" { default = "" }